# TODO

请为 `UserController` 实现如下的 API

| Key            | Description                                                           |
|----------------|-----------------------------------------------------------------------|
| URI            | `/users/<id>`                                                         |
| Status Code    | 若成功则为 `200`，如果用户不存在为 `404`                                   |
| Content-Type   | `application/json`                                                    |
| Content        | `{"firstName": "<first name>", "lastName": "<last name>"}`            |

**注意**：即便你实现了 API，但是测试还是会挂（太惨了）。请在该工程中必要的类型上添加 annotation，或者添加新的方法，令测试全部通过。

**注意**：**不允许修改现有代码！！**